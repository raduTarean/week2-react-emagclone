import React from "react";
import { BrowserRouter } from "react-router-dom";

import "./App.css";
import ProductCard from "./components/ProductCard";
import EmagAnnouncementsSection from "./composites/EmagAnnoucementsSection";
import EmagMainNavbar from "./composites/EmagMainNavbar";
import EmagMegaMenu from "./composites/EmagMegaMenu";
import EmagSecondaryNavbar from "./composites/EmagSecondaryNavbar";

const slideShowImages = [
  {
    imageSource: "./images/134643.webp",
    imageAlt: "rabla",
  },
  {
    imageSource: "./images/135058.webp",
    imageAlt: "mosul",
  },
];

const secondaryNavLinks = [
  {
    icon: "fas fa-bars",
    title: "Produse",
    toLink: "/produse",
  },
  {
    title: "Genius",
    toLink: "/genius",
  },
  {
    title: "Rabla eMAG",
    toLink: "/rabla",
  },
  {
    title: "Card eMAG",
    toLink: "/card",
  },
  {
    title: "Resigilate",
    toLink: "/resigilate",
  },
  {
    icon: "fas fa-headphones",
    title: "eMAG Help",
    toLink: "/help",
  },
];
const megaMenuLinks = [
  {
    icon: "fa-solid fa-mobile-screen-button",
    title: "Laptop, tablete, telefoane",
    toLink: "/laptops",
  },
  {
    icon: "fa-solid fa-computer-mouse",
    title: "PC, Periferice & Software",
    toLink: "/pc",
  },
  {
    icon: "fa-solid fa-tv",
    title: "TV, Audio-Video & Foto",
    toLink: "/tv",
  },
  {
    icon: "fa-solid fa-blender-phone",
    title: "Electrocasnice & Climatizare",
    toLink: "/electrocasnice",
  },
  {
    icon: "fa-solid fa-gamepad",
    title: "Gaming, Carti & Biblioteca",
    toLink: "/gaming",
  },
  {
    icon: "fas fa-bag-shopping",
    title: "Bacanie",
    toLink: "/bacanie",
  },
  {
    icon: "fas fa-shirt",
    title: "Fashion",
    toLink: "/fashion",
  },
  {
    icon: "fas fa-person",
    title: "Ingrijire personala & Cosmetice",
    toLink: "/help",
  },
  {
    icon: "fas fa-house",
    title: "Casa, Gradina, Bricolak",
    toLink: "/casa",
  },
  {
    icon: "fas fa-baseball",
    title: "Sport & Activitati in Aer Liber",
    toLink: "/sport",
  },
  {
    icon: "fas fa-car",
    title: "Auto, Moto, RCA",
    toLink: "/auto",
  },
  {
    icon: "fas fa-chess-pawn",
    title: "Jucarii, Copii & Bebe",
    toLink: "/help",
  },
];

const announcements = [
  {
    title: "Genius se extinde!",
    message: "Acum ai beneficii in eMAG, Tazz, Fashion Days si Freshfull",
    ctaLink: "/genius",
    imageSource: "./images/81213.webp",
    imageAlt: "rabla",
    cta: "Vezi detalii",
  },
  {
    title: "Alege easyBox",
    message: "Livrare asa cum vrei, langa casa ta, disponibil 24/7",
    ctaLink: "/easybox",
    imageSource: "./images/123359.webp",
    imageAlt: "rabla",
    cta: "Vezi detalii",
  },
  {
    title: "Doneaza pentru elevi!",
    message: "Doneaza si ofera-le timp celor din clasa care nu se lasa",
    ctaLink: "/doneaza",
    imageSource: "./images/130445.webp",
    imageAlt: "rabla",
    cta: "Ajuta si tu!",
  },
];

const productCards = [
  {
    imageSource: "./images/res_bcbad0ac82e0fbfa717f892bf45248bc.webp",
    imageAlt: "piscina",
    title: "Piscina Intex Metal Frame 305X65",
    price: 1500,
    prp: 2000,
    rating: 2.5,
    ratingCount: 30,
    link: "/piscina-intex-metal-frame-305x65",
  },
];

function App() {
  return (
    <>
      <BrowserRouter>
        <EmagMainNavbar />
        <EmagSecondaryNavbar links={secondaryNavLinks} />
        <EmagMegaMenu
          className="megaMenu"
          links={megaMenuLinks}
          images={slideShowImages}
        />
        <EmagAnnouncementsSection announcements={announcements} />
      </BrowserRouter>
      <ProductCard className="test-card" {...productCards[0]} />
    </>
  );
}

export default App;
