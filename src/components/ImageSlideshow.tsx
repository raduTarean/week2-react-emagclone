import React, { FunctionComponent, useState } from "react";
import FaIcon from "./FaIcon";
import { ImageProps } from "./Image";

type ImageSlideshowProps = {
  images: ImageProps[];
  className?: string;
};

const slideStyles = {
  width: "100%",
  height: "100%",
  backgroundSize: "auto",
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
};

const rightArrowStyles = {
  position: "absolute",
  top: "50%",
  transform: "translate(0, -50%)",
  right: "32px",
  fontSize: "45px",
  color: "#000",
  zIndex: 1,
  cursor: "pointer",
} as const;

const leftArrowStyles = {
  position: "absolute",
  top: "50%",
  transform: "translate(0, -50%)",
  left: "32px",
  fontSize: "45px",
  color: "#000",
  zIndex: 1,
  cursor: "pointer",
} as const;

const sliderStyles = {
  position: "relative",
  height: "100%",
} as const;

const dotsContainerStyles = {
  display: "flex",
  position: "absolute",
  bottom: "1rem",
  right: "10%",
  justifyContent: "center",
  color: "#000",
} as const;

const dotStyle = {
  margin: "0 3px",
  cursor: "pointer",
  fontSize: "20px",
  width: "30px",
  height: "30px",
  borderRadius: "50%",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  textAlign: "center",
  border: "inset 2px solid #fff",
} as const;

const ImageSlideshow: FunctionComponent<ImageSlideshowProps> = ({
  images,
  className,
}) => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const goToPrevious = () => {
    const isFirstSlide = currentIndex === 0;
    const newIndex = isFirstSlide ? images.length - 1 : currentIndex - 1;
    setCurrentIndex(newIndex);
  };
  const goToNext = () => {
    const isLastSlide = currentIndex === images.length - 1;
    const newIndex = isLastSlide ? 0 : currentIndex + 1;
    setCurrentIndex(newIndex);
  };
  const goToSlide = (slideIndex: number) => {
    setCurrentIndex(slideIndex);
  };
  const slideStylesWidthBackground = {
    ...slideStyles,
    backgroundImage: `url(${images[currentIndex].imageSource})`,
  };
  return (
    <div className={className} style={sliderStyles}>
      <div>
        <div onClick={goToPrevious} style={leftArrowStyles}>
          <FaIcon icon="fas fa-chevron-left" />
        </div>
        <div onClick={goToNext} style={rightArrowStyles}>
          <FaIcon icon="fas fa-chevron-right" />
        </div>
      </div>
      <div style={slideStylesWidthBackground}></div>
      <div style={dotsContainerStyles}>
        {images.map((slide, slideIndex) => (
          <div
            style={dotStyle}
            key={slideIndex}
            onClick={() => goToSlide(slideIndex)}
          >
            <FaIcon icon="fas fa-circle" />
          </div>
        ))}
      </div>
    </div>
  );
};

export default ImageSlideshow;
