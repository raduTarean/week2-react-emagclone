import React, { FunctionComponent } from "react";

type FaIconProps = {
  icon: string;
};

const FaIcon: FunctionComponent<FaIconProps> = ({ icon }) => {
  return <i className={icon}></i>;
};

export default FaIcon;
