import React, { FunctionComponent } from "react";
import { NavLinkProps } from "./NavLink";
import NavLink from "./NavLink";

type LinkMenuProps = {
  className?: string;
  links: NavLinkProps[];
};

const LinkMenu: FunctionComponent<LinkMenuProps> = ({ className, links }) => {
  return (
    <ul className={className}>
      {links.map((link, i) => {
        return (
          <li key={i}>
            <NavLink icon={link.icon} title={link.title} toLink={link.toLink} />
          </li>
        );
      })}
    </ul>
  );
};

export default LinkMenu;
