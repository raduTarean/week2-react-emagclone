import React, { FunctionComponent } from "react";
import { Link, To } from "react-router-dom";
import FaIcon from "./FaIcon";

export type NavLinkProps = {
  toLink: To;
  title: string | undefined;
  className?: string;
  icon?: string;
};

const NavLink: FunctionComponent<NavLinkProps> = ({
  toLink,
  title,
  className,
  icon,
}) => {
  return (
    <Link className={className} to={toLink}>
      {icon && <FaIcon icon={icon}></FaIcon>}
      {title}
    </Link>
  );
};

export default NavLink;
