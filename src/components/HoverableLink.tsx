import React, { FunctionComponent, useState } from "react";
import { Link } from "react-router-dom";
import FaIcon from "./FaIcon";

type HoverableLinkProps = {
  children: React.ReactNode;
  title: string;
  icon?: string;
  link: string;
  className?: string;
};

const HoverableLink: FunctionComponent<HoverableLinkProps> = ({
  children,
  title,
  icon,
  link,
  className,
}) => {
  const [isHovered, setIsHovered] = useState(false);
  const onMouseEnterHandler = () => {
    setIsHovered(true);
  };
  const onMouseLeaveHandler = () => {
    setIsHovered(false);
  };

  return (
    <div
      className={className}
      onMouseEnter={onMouseEnterHandler}
      onMouseLeave={onMouseLeaveHandler}
    >
      <Link className="hoverableLink__link" to={link}>
        {icon && <FaIcon icon={icon}></FaIcon>}
        <span>{title}</span>
        <FaIcon icon="fa-solid fa-chevron-down"></FaIcon>
      </Link>
      {isHovered && (
        <div className="dropdown">
          <div className="dropdown-content">{children}</div>
        </div>
      )}
    </div>
  );
};

export default HoverableLink;
