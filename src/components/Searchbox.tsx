import React, { FunctionComponent } from "react";
import FaIcon from "./FaIcon";

type SearchboxProps = {
  className?: string;
};

const Searchbox: FunctionComponent<SearchboxProps> = ({ className }) => {
  return (
    <div className={className}>
      <input type="text" />
      <FaIcon icon="fa-solid fa-magnifying-glass"></FaIcon>
    </div>
  );
};

export default Searchbox;
