import React, { FunctionComponent } from "react";
import FaIcon from "./FaIcon";

export type ProductCardProps = {
  title: string;
  price: number;
  prp: number;
  imageSource: string;
  imageAlt?: string;
  link: string;
  className?: string;
  rating: number;
  ratingCount: number;
};

const map = (
  val: number,
  in_min: number,
  in_max: number,
  out_min: number,
  out_max: number
) => ((val - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min;

const ProductCard: FunctionComponent<ProductCardProps> = ({
  className,
  imageSource,
  imageAlt,
  price,
  prp,
  title,
  rating,
  ratingCount,
}) => {
  const ratingPercentage = map(rating, 1, 5, 20, 100);
  return (
    <div className={className}>
      <div className={className + "__header"}>
        <img src={imageSource} alt={imageAlt || "Product Image"} />
        <span>{title}</span>
      </div>
      <div className={className + "__rating"}>
        <div
          style={{
            WebkitBackgroundClip: "text",
            backgroundClip: "text",
            color: "transparent",
            width: "fit-content",
            backgroundImage: `linear-gradient(90deg,yellow ${ratingPercentage}%,black ${
              ratingPercentage + 0.5
            }%)`,
          }}
          className={className + "__rating__stars"}
        >
          <FaIcon icon="fas fa-star"></FaIcon>
          <FaIcon icon="fas fa-star"></FaIcon>
          <FaIcon icon="fas fa-star"></FaIcon>
          <FaIcon icon="fas fa-star"></FaIcon>
          <FaIcon icon="fas fa-star"></FaIcon>
        </div>

        <span>{rating}</span>
        <span>({ratingCount})</span>
      </div>
      <div className={className + "__body"}>
        <span>{price}</span>
        <span>{prp}</span>
        <button>
          <FaIcon icon="fas fa-cart-shopping" />
        </button>
      </div>
    </div>
  );
};

export default ProductCard;
