import React, { FunctionComponent } from "react";
import { Link } from "react-router-dom";

export type AnnouncementProps = {
  title: string;
  message: string;
  className?: string;
  imageSource?: string;
  imageAlt?: string;
  cta: string;
  ctaLink: string;
};

const Announcement: FunctionComponent<AnnouncementProps> = ({
  className,
  imageSource,
  imageAlt,
  title,
  message,
  ctaLink,
  cta,
}) => {
  return (
    <div className={className}>
      <div className={className + "__header"}>
        {imageSource && (
          <img src={imageSource} alt={imageAlt || "announcement image"} />
        )}
        <span>{title}</span>
      </div>
      <div className={className + "__body"}>{message}</div>
      <div className={className + "__cta"}>
        <Link to={ctaLink}>{cta}</Link>
      </div>
    </div>
  );
};

export default Announcement;
