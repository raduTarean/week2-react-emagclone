import React, { FunctionComponent } from "react";
import ProductCard, { ProductCardProps } from "./ProductCard";

type ProductCardListProps = {
  className?: string;
  products: ProductCardProps[];
  title: string;
};

const ProductCardList: FunctionComponent<ProductCardListProps> = ({
  className,
  products,
  title,
}) => {
  return (
    <section className={className}>
      <h3>{title}</h3>
      <div className={className + "__list"}>
        {products.map((product, index) => {
          return <ProductCard key={index} {...product} />;
        })}
      </div>
    </section>
  );
};

export default ProductCardList;
