import React, { FunctionComponent } from "react";

export type ImageProps = {
  imageSource: string;
  imageAlt: string;
  className?: string;
};

const Image: FunctionComponent<ImageProps> = ({
  className,
  imageSource,
  imageAlt,
}) => {
  return (
    <div className={className}>
      <img src={imageSource} alt={imageAlt} />
    </div>
  );
};

export default Image;
