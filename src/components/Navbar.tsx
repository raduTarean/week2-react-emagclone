import React, { FunctionComponent } from "react";

type NavbarProps = {
  children: React.ReactNode;
  className?: string;
};

const Navbar: FunctionComponent<NavbarProps> = (props) => {
  return <nav className={props.className}>{props.children}</nav>;
};

export default Navbar;
