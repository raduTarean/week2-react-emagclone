import React, { FunctionComponent } from "react";
import Announcement, { AnnouncementProps } from "../../components/Announcement";
import "./style.css";

type EmagAnnouncementsSectionProps = {
  announcements: AnnouncementProps[];
};

const EmagAnnouncementsSection: FunctionComponent<
  EmagAnnouncementsSectionProps
> = ({ announcements }) => {
  return (
    <section className="eMagAnnouncements">
      {announcements.map((announcement, index) => {
        return (
          <Announcement
            key={index}
            className="eMagAnnouncements_announcement"
            {...announcement}
          />
        );
      })}
    </section>
  );
};

export default EmagAnnouncementsSection;
