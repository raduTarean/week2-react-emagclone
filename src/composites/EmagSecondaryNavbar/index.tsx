import React, { FunctionComponent } from "react";
import Navbar from "../../components/Navbar";
import NavLink from "../../components/NavLink";
import "./style.css";
import { NavLinkProps } from "../../components/NavLink";

type EmagSecondaryNavbarProps = {
  className?: string;
  links: NavLinkProps[];
};

const EmagSecondaryNavbar: FunctionComponent<EmagSecondaryNavbarProps> = ({
  className,
  links,
}) => {
  return (
    <Navbar className="emagNavSec">
      <ul className="emagNavSec__links">
        {links.map((link, i) => {
          return (
            <li key={i}>
              <NavLink
                icon={link.icon}
                title={link.title}
                toLink={link.toLink}
              />
            </li>
          );
        })}
      </ul>
    </Navbar>
  );
};

export default EmagSecondaryNavbar;
