import React from "react";
import HoverableLink from "../../components/HoverableLink";
import Image from "../../components/Image";
import Navbar from "../../components/Navbar";
import Searchbox from "../../components/Searchbox";
import "./style.css";

const EmagMainNavbar = () => {
  return (
    <Navbar className="emagNav">
      <Image
        className="emagNav__logo"
        imageSource="./images/88362.svg"
        imageAlt="eMAG"
      />
      <Searchbox className="emagNav__searchBox" />
      <HoverableLink
        className="emagNav__hoverLink"
        title="Contul Meu"
        icon="fa-solid fa-user"
        link="/account"
      >
        <span>Contul Meu</span>
      </HoverableLink>
      <HoverableLink
        className="emagNav__hoverLink"
        title="Favorite"
        icon="fa-regular fa-heart"
        link="/favorites"
      >
        <span>Favorite</span>
      </HoverableLink>
      <HoverableLink
        className="emagNav__hoverLink"
        title="Cosul Meu"
        icon="fa-solid fa-cart-shopping"
        link="/cart"
      >
        <span>Cosul</span>
      </HoverableLink>
    </Navbar>
  );
};

export default EmagMainNavbar;
