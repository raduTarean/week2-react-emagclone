import React, { FunctionComponent } from "react";
import { ImageProps } from "../../components/Image";
import LinkMenu from "../../components/LinkMenu";
import { NavLinkProps } from "../../components/NavLink";
import ImageSlideshow from "../../components/ImageSlideshow";
import "./style.css";
type EmagMegaMenuProps = {
  className?: string;
  links: NavLinkProps[];
  images: ImageProps[];
};

const EmagMegaMenu: FunctionComponent<EmagMegaMenuProps> = ({
  className,
  links,
  images,
}) => {
  return (
    <div className={className}>
      <div className={className + "__linkMenu-container"}>
        <LinkMenu className={className + "__linkMenu"} links={links} />
      </div>
      <div className={className + "__slideShow-container"}>
        <ImageSlideshow className={className + "__slideShow"} images={images} />
      </div>
    </div>
  );
};

export default EmagMegaMenu;
